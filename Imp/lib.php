<?php
	require_once(realpath(__DIR__.'/../config.php'));

	function redirect($url) {
		global $CFG;
		header('Location: '.$CFG->clientURL.$url);
		die();
	}

	function json_redirect($url) {
		echo json_encode([
			'redirect' => $url
		]);
		die();
	}

	function setHeader() {
		global $CFG;
		header('Access-Control-Allow-Origin: '.$CFG->clientURL);
		header('Access-Control-Allow-Credentials: true');
		header('Content-Type: application/json');
		header('Access-Control-Allow-Headers: Content-Type');

		if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
			die();
		}
	}

	function checkLogin($isAjax = false) {
		if(!SM::isSESSION('cpf')) {
			if(!$isAjax) {
				redirect('/inicio-submissao');
			} else {
				echo json_encode([
					'redirect' => '/inicio-submissao'
				]);
				die();
			}
		}
	}

	function checkAdminLogin($isAjax = false) {
		if(!SM::isSESSION('admin')) {
			if(!$isAjax) {
				redirect('/admin');
			} else {
				echo json_encode([
					'redirect' => '/admin'
				]);
				die();
			}
		}
	}

	function validateDate($date, $format = 'Y-m-d H:i:s') {
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

	function validaCPF($cpf) {
		// Verifica se foi informado todos os digitos corretamente
		if (strlen($cpf) != 11) {
			return false;
		}
		// Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
		if (preg_match('/(\d)\1{10}/', $cpf)) {
			return false;
		}
		// Faz o calculo para validar o CPF
		for ($t = 9; $t < 11; $t++) {
			for ($d = 0, $c = 0; $c < $t; $c++) {
				$d += $cpf{$c} * (($t + 1) - $c);
			}
			$d = ((10 * $d) % 11) % 10;
			if ($cpf{$c} != $d) {
				return false;
			}
		}
		return true;
	}
