<?php
    namespace inscricao_graduacao {
        use ConnDB;
        use SM;
        use SendMail;

        class CDB extends ConnDB {
            function __construct($dblib = 'mysql') {
                parent::__construct($dblib);
            }

            function submitInscription(
                $userid, 
                $best_degree, 
                $graduation_for, 
                $pos_graduation_for, 
                $employedbefore, 
                $teacherexperience, 
                $eadexperience,
                $contentexperience,
                $otherexperiences,
                $roles,
                $subjects,
                $files,
                $otherFiles
            ) {
                $this->beginTransaction();

                // Insere os arquivos e retorna o id deles
                $filesid = [];
                foreach($files as $key=>$file) {
                    if ($this->insertFile($file)) {
                        $filesid[$key] = $this->lastInsertID();
                    } else {
                        $this->rollBack();
                        return false;
                    }
                }

                // Insere os outros arquivos e retorna o id deles
                $otherfilesid = [];
                foreach($otherFiles as $file) {
                    if ($this->insertFile($file)) {
                        $otherfilesid[] = $this->lastInsertID();
                    } else {
                        $this->rollBack();
                        return false;
                    }
                }

                // Cria a inscrição com os dados informados
                $sql = "INSERT INTO graduation_inscription (
                    id,
                    user_id,
                    best_degree,
                    graduation_for,
                    pos_graduation_for,
                    employed_before,
                    teacher_experience,
                    ead_experience,
                    content_manager_experience,
                    other_experiences,
                    lattes_file_id,
                    rg_file_id,
                    cpf_file_id,
                    elector_file_id,
                    residence_file_id,
                    graduation_file_front_id,
                    graduation_file_verse_id,
                    pos_graduation_file_front_id,
                    pos_graduation_file_verse_id
                ) VALUES (
                    default, 
                    :userid, 
                    :best_degree, 
                    :graduation_for,
                    :pos_graduation_for,
                    :employeedbefore,
                    :teacherexperience,
                    :eadexperience,
                    :contentmanagerexperience,
                    :otherexperiences,
                    :lattes_file,
                    :rg_file_front,
                    :cpf_file,
                    :elector_file,
                    :residence_file,
                    :graduation_file_front,
                    :graduation_file_verse,
                    :posgraduation_file_front,
                    :posgraduation_file_verse
                )";

                $stmt = $this->prepare($sql, [
                    'userid' => $userid,
                    'best_degree' => $best_degree,
                    'graduation_for' => $graduation_for,
                    'pos_graduation_for' => $pos_graduation_for,
                    'employeedbefore' => 'yes' ? 1 : 0,
                    'teacherexperience' => $teacherexperience,
                    'eadexperience' => $eadexperience,
                    'contentmanagerexperience' => $contentexperience,
                    'otherexperiences' => $otherexperiences,
                    'lattes_file' => $filesid['lattes'],
                    'rg_file_front' => $filesid['rg'],
                    'cpf_file' => $filesid['residence'],
                    'elector_file' => $filesid['elector'],
                    'residence_file' => $filesid['residence'],
                    'graduation_file_front' => $filesid['graduationFront'],
                    'graduation_file_verse' => isset($filesid['graduationVerse']) ? $filesid['graduationVerse'] : NULL,
                    'posgraduation_file_front' => isset($filesid['posgraduationFront']) ? $filesid['posgraduationFront'] : NULL,
                    'posgraduation_file_verse' => isset($filesid['posgraduationVerse']) ? $filesid['posgraduationVerse'] : NULL
                ]);

                try {
                    $stmt->execute();
                } catch(Exception $e) {
                    $this->rollBack();
                    return false;
                }

                $inscriptionid = $this->lastInsertID();

                // Relaciona as roles com a inscrição da graduação
                foreach($roles as $role) {
                    $roleid = $this->getRoleIdFromName($role);
                    if (!$roleid || !$this->assignRoleToGraduationInscription($inscriptionid, $roleid)) {
                        $this->rollBack();
                        return false;
                    }
                }

                // Relaciona as disciplinas com a inscrição da graduação
                foreach($subjects as $subject) {
                    if (!$this->assignCourseToGraduationInscription($subject->courseid, $inscriptionid)) {
                        $this->rollBack();
                        return false;
                    }
                }

                // Relaciona os arquivos extras com a inscrição da graduação
                foreach ($otherfilesid as $id) {
                    if (!$this->assignOtherFileToGraduationInscription($id, $inscriptionid)) {
                        $this->rollBack();
                        return false;
                    }
                }

                if(!SendMail(
                    SM::getSESSION('user')['email'], 
                    SM::getSESSION('user')['name'], 
                    'Confirmação de inscrição para Graduação - UNIFACEX', 
                    'Sua inscrição foi realizada com sucesso! Aguarde por novas orientações.',
                    false,
                    'UNIFACEX')
                ) {
                    $this->rollBack()();
                    return false;
                }

                $this->commit();
                return true;
            }

            function assignRoleToGraduationInscription($graduationinscriptionid, $roleid) {
                $sql = "INSERT INTO graduation_inscription_role (graduation_inscription_id, role_id) VALUES (
                    :graduationinscriptionid,
                    :roleid
                )";

                $stmt = $this->prepare($sql, ['graduationinscriptionid' => $graduationinscriptionid, 'roleid' => $roleid]);

                try {
                    $stmt->execute();
                } catch (Exception $e) {
                    return false;
                }

                return true;
            }

            function getRoleIdFromName($name) {
                if($name === 'both') {
                    $sql = "SELECT id FROM role WHERE name = 'conteudist' OR name = 'reviewer'";
                    $stmt = $this->prepare($sql);
                } else {
                    $sql = "SELECT id FROM role WHERE name = :name";
                    $stmt = $this->prepare($sql, ['name' => $name]);
                }
                try {
                    $stmt->execute();
                } catch (Exception $e) {
                    return NULL;
                }

                $aux = $stmt->fetch();
                return $aux ? $aux->id : NULL;
            }

            function insertFile($file): bool {
                $separatedarquivo = explode('.', $file['name']);
                $filebasename = implode('', array_slice($separatedarquivo, 0, -1));
                $fileextension = array_pop($separatedarquivo);
                $token = bin2hex(random_bytes(16));

                // Salva o arquivo na pasta upload:
                $uploaddir = realpath(__DIR__.'/../../upload');
                if (move_uploaded_file($file['tmp_name'], $uploaddir.'/'.$token)) {
                    $sql = "INSERT INTO file VALUES (default, :basename, :extension, :token)";
                    $stmt = $this->prepare($sql, [
                        'basename' => $filebasename,
                        'extension' => $fileextension,
                        'token' => $token
                    ]);
                    if ($stmt->execute()) {
                        return true;
                    }
                }

                return false;
            }

            function assignCourseToGraduationInscription($courseId, $inscriptionId) {
                $sql = "INSERT INTO graduation_inscription_course (graduation_course_id, graduation_inscription_id) VALUES (
                    :courseid,
                    :graduationinscriptionid
                )";

                $stmt = $this->prepare($sql, ['courseid' => $courseId, 'graduationinscriptionid' => $inscriptionId]);
                try {
                    $stmt->execute();
                } catch(Exception $e) {
                    return false;
                }

                return true;
            }

            function assignOtherFileToGraduationInscription($otherFileId, $graduationInscriptionId): bool {
                $sql = "INSERT INTO graduation_inscription_file (graduation_inscription_id, file_id) VALUES (
                    :graduationinscriptionid,
                    :fileid
                )";
                $stmt = $this->prepare($sql, ['graduationinscriptionid' => $graduationInscriptionId, 'fileid' => $otherFileId]);
                try {
                    $stmt->execute();
                } catch(Exception $e) {
                    return false;
                }

                return true;
            }
        }
    }