<?php
    namespace convert {
        use ConnDB;

        class SCDB extends ConnDB {
            function __construct($dblib = 'sqlite') {
                parent::__construct($dblib);
            }

            function getSubmissions() {
                $sql = "SELECT * FROM submission ORDER BY name";
                $stmt = $this->prepare($sql);
                $stmt->execute();
                return $stmt->fetchAll();
            }
        }

        class CDB extends ConnDB {
            function __construct($dblib = 'mysql') {
                parent::__construct($dblib);
            }

            function getSubmissions() {
                $sql = "SELECT * FROM graduation_submission ORDER BY name";
                $stmt = $this->prepare($sql);
                try {
                    $stmt->execute();
                } catch (Exception $e) {
                    return NULL;
                }
                
                return $stmt->fetchAll();
            }
        }
    }