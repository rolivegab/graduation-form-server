<?php
    namespace cadastro {
        require_once( realpath(__DIR__.'/../../config.php'));
        use ConnDB;
        use SendMail;
        use stdClass;
        use ob_start;
        use ob_get_clean;

        class CDB extends ConnDB {
            function __construct($dblib = 'mysql') {
                parent::__construct($dblib);
            }

            function isCPFExists($cpf) {
                $sql = "SELECT 1 FROM user WHERE cpf=:cpf";
                $stmt = $this->prepare($sql, ['cpf' => $cpf]);
                $stmt->execute();
                return $stmt->fetch() !== false;
            }

            function isEmailExists($email) {
                $sql = "SELECT 1 as flag FROM user WHERE email=:email";
                $stmt = $this->prepare($sql, ['email' => $email]);
                $stmt->execute();
                return $stmt->fetch() !== false;
            }

            function createUser(stdClass $p) {
                global $CFG;
                // Inicia a transação:
                $this->beginTransaction();

                // Cria o usuário:
                $sql = "INSERT INTO user (id, cpf, email, time_created) VALUES (default, :cpf, :email, UNIX_TIMESTAMP())";
                $stmt = $this->prepare($sql, ['cpf' => $p->cpf, 'email' => $p->email]);
                $p2 = clone($p);
                unset($p2->cpf);
                unset($p2->email);
                if ($stmt->execute()) {
                    $userid = $this->lastInsertID();

                    $sql = "INSERT INTO user_info (
                        user_id,
                        name, 
                        rg, 
                        birthday, 
                        address, 
                        number, 
                        cep, 
                        city, 
                        tel
                    ) VALUES (
                        :userid,
                        :name,
                        :rg,
                        UNIX_TIMESTAMP(STR_TO_DATE(:birthday, '%d/%m/%Y')),
                        :address,
                        :number,
                        :cep,
                        :city,
                        :tel
                    )";

                    $p2->userid = $userid;
                    $stmt = $this->prepare($sql, $p2);
                    if ($stmt->execute()) {
                        // Coloca ele na lista de emails:
                        $token = bin2hex(random_bytes(16));
                        $validated = false;
                        $sql = "INSERT INTO user_email_confirmation (user_id, token, validated, time_created) VALUES (:userid, :token, :validated, UNIX_TIMESTAMP())";
                        $stmt = $this->prepare($sql, [
                            'userid' => $userid,
                            'token' => $token,
                            'validated' => $validated
                        ]);
                        if ($stmt->execute()) {
                            $url = $CFG->clientURL.'/cadastro/confirmacao?token='.$token;
                            ob_start();
                            require_once( realpath(__DIR__.'/../email/email-validation.php'));
                            $email = ob_get_clean();
                            if (SendMail(
                                $p->email, 
                                $p->name,
                                'Confirmação de email - UNIFACEX',
                                $email,
                                true,
                                'UNIFACEX'
                            )) {
                                $this->commit();
                                return true;
                            }
                        }
                    }
                }

                $this->rollBack();
                return false;
            }

            function checkToken(string $token): bool {
                $sql = "SELECT 1 as flag FROM user_email_confirmation WHERE token = :token";
                $stmt = $this->prepare($sql, ['token' => $token]);
                $stmt->execute();
                return $stmt->fetch() !== false;
            }

            function setValidate(string $token, bool $value): bool {
                $sql = "UPDATE user_email_confirmation SET validated = :value WHERE token = :token";
                $stmt = $this->prepare($sql, ['token' => $token, 'value' => $value]);
                $stmt->execute();
                return $stmt->rowCount();
            }
        }
    }