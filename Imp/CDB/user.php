<?php
    namespace user;
    use ConnDB;
    use SendMail;

    class CDB extends ConnDB {
        function __construct($dblib = 'mysql') {
            parent::__construct($dblib);
        }

        function getRolesFromUser($userid) {
            $sql = "SELECT name 
            FROM role 
            LEFT JOIN user_role ON (user_role.role_id = role.id)
            WHERE user_role.user_id = :userid";
            $stmt = $this->prepare($sql, ['userid' => $userid]);
            if ($stmt->execute()) {
                return $stmt->fetchAll();
            }
        }

        function isEmailValidated($userid) {
            $sql = "SELECT validated FROM user_email_confirmation WHERE user_id = :userid";
            $stmt = $this->prepare($sql, ['userid' => $userid]);
            if ($stmt->execute()) {
                return $stmt->fetch()->validated === '1' ? true : false;
            }
        }

        /**
            * Veririca se o cpf com o email são válidos como login de usuário.
            *
            * @param string $cpf
            * @param string $email
            * @return string|void retorna o id do usuário, ou vazio.
            */
        function checkLogin($cpf, $email) {
            $sql = "SELECT id FROM user WHERE cpf = :cpf AND email = :email";
            $stmt = $this->prepare($sql, ['cpf' => $cpf, 'email' => $email]);
            if ($stmt->execute() && $row = $stmt->fetch()) {
                return $row->id;
            }

            return null;
        }

        function getMail($userid) {
            $sql = "SELECT email FROM user WHERE id = :id";
            $stmt = $this->prepare($sql, ['id' => $userid]);
            try {
                $stmt->execute();
            } catch (Exception $e) {
                return NULL;
            }

            if ($stmt->rowCount() > 0) {
                return $stmt->fetch()->email;
            }
        }

        function getName($userid) {
            $sql = "SELECT name FROM user LEFT JOIN user_info ON (user_info.user_id = user.id) WHERE id = :userid";
            $stmt = $this->prepare($sql, ['userid' => $userid]);
            try {
                $stmt->execute();
            } catch (Exception $e) {
                return NULL;
            }

            if ($stmt->rowCount() > 0) {
                return $stmt->fetch()->name;
            }
        }

        function hasGraduationInscription($userid) {
            $sql = "SELECT 1 FROM graduation_inscription LEFT JOIN user ON user.id = graduation_inscription.user_id WHERE user.id = :userid LIMIT 1";
            $stmt = $this->prepare($sql, ['userid' => $userid]);
            try{
                $stmt->execute();
            } catch (Exception $e) {
                return NULL;
            }

            return $stmt->rowCount() > 0;
        }

        function getUsers() {
            $sql = "SELECT * FROM user";
            $stmt = $this->prepare($sql);
            try{
                $stmt->execute();
            } catch (Exception $e) {
                return NULL;
            }

            return $stmt->fetchAll();
        }
    }