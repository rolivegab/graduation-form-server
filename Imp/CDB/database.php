<?php
    namespace database {
        use ConnDB;

        class CDB extends ConnDB {
            function __construct($dblib = 'mysql') {
                parent::__construct($dblib);
            }

            function insertSubjects($areas) {
                $this->beginTransaction();
                $success = true;

                foreach($areas as $area=>$courses) {
                    if ($this->_insertGraduationArea($area)) {
                        $areaid = $this->lastInsertID();

                        foreach($courses as $course) {
                            if ($this->_insertGraduationCourse($course)) {
                                $courseid = $this->lastInsertID();
                                if (!$this->_insertGradiationAreaCourse($areaid, $courseid)) {
                                    $success = false;
                                }
                            } else {
                                $success = false;
                            }
                        }
                    } else {
                        $success = false;
                    }
                }

                if ($success) {
                    $this->commit();
                } else {
                    $this->rollBack();
                }
            }

            function _insertGraduationArea($name) {
                $sql = "INSERT INTO graduation_area VALUES (default, :name)";
                $stmt = $this->prepare($sql, ['name' => $name]);
                return $stmt->execute();
            }

            function _insertGraduationCourse($name) {
                $sql = "INSERT INTO graduation_course VALUES (default, :name)";
                $stmt = $this->prepare($sql, ['name' => $name]);
                return $stmt->execute();
            }

            function _insertGradiationAreaCourse($areaid, $courseid) {
                $sql = "INSERT INTO graduation_area_course(graduation_area_id, graduation_course_id) VALUES (:areaid, :courseid)";
                $stmt = $this->prepare($sql, ['areaid' => $areaid, 'courseid' => $courseid]);
                return $stmt->execute();
            }
        }
    }