<?php
    namespace area {
        use ConnDB;

        class CDB extends ConnDB {
            function __construct($dblib = 'mysql') {
                parent::__construct($dblib);
            }

            function getGraduationAreas() {
                $sql = "SELECT id, name FROM graduation_area";
                $stmt = $this->prepare($sql);
                $stmt->execute();
                return $stmt->fetchAll();
            }

            function getGraduationCoursesFromArea($graduationareaid, $roles, $limit) {
                $role_ids = [];
                foreach($roles as $key=>$role) {
                    if ($role === 'conteudist' || $role === 'reviewer') {
                        $roleid = $this->getRoleIdFromName($role);
                        $role_ids[] = $roleid;
                    }
                }
                $roleQuery = implode(', ', $role_ids);

                $sql = "SELECT
                graduation_course.id,
                graduation_course.name,
                (
                    CASE
                        WHEN SUM(tb2.has_role) > :limit THEN
                            0
                        ELSE
                            1
                    END
                ) as active,
                SUM(tb2.has_role) as num_inscription
                FROM
                graduation_course
                LEFT JOIN graduation_inscription_course ON graduation_inscription_course.graduation_course_id = graduation_course.id
                LEFT JOIN
                (
                    SELECT
                    tb1.id,
                    (
                        CASE 
                            WHEN SUM(tb1.role_id) IS NOT NULL THEN
                                1
                            ELSE
                                0
                        END
                    ) as has_role
                    FROM
                    (
                        SELECT DISTINCT (graduation_inscription.id),
                        role_id
                        FROM
                            graduation_inscription
                        LEFT JOIN
                            graduation_inscription_role ON graduation_inscription_role.graduation_inscription_id = graduation_inscription.id
                        WHERE graduation_inscription_role.role_id IS NOT NULL
                        AND graduation_inscription_role.role_id IN ($roleQuery)
                    ) as tb1
                    GROUP BY tb1.id
                ) as tb2 ON tb2.id = graduation_inscription_course.graduation_inscription_id
                LEFT JOIN graduation_area_course ON graduation_area_course.graduation_course_id = graduation_course.id
                WHERE graduation_area_course.graduation_area_id = :graduationareaid
                GROUP BY graduation_course.id";
                $stmt = $this->prepare($sql, ['limit' => $limit, 'graduationareaid' => $graduationareaid]);
                $stmt->execute();
                return $stmt->fetchAll();
            }

            function getRoleIdFromName($name) {
                if($name === 'both') {
                    $sql = "SELECT id FROM role WHERE name = 'conteudist' OR name = 'reviewer'";
                    $stmt = $this->prepare($sql);
                } else {
                    $sql = "SELECT id FROM role WHERE name = :name";
                    $stmt = $this->prepare($sql, ['name' => $name]);
                }
                try {
                    $stmt->execute();
                } catch (Exception $e) {
                    return NULL;
                }

                $aux = $stmt->fetch();
                return $aux ? $aux->id : NULL;
            }
        }
    }