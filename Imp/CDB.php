<?php
    namespace inicioSubmissao {
        use ConnDB;

        class CDB extends ConnDB {
            function __construct($file = NULL) {
                parent::__construct($file);
            }

            function checkCPF($cpf) {
                $sql = "SELECT 0 FROM submission WHERE cpf = :cpf";
                $stmt = $this->prepare($sql, ['cpf' => $cpf]);
                $stmt->execute();
                return $stmt->fetch();
            }   
        }
    }

    namespace enviarConteudo {
        require_once( realpath(__DIR__.'/../Core/ConnDB.php'));
        use ConnDB;

        class CDB extends ConnDB {
            function __construct($file = NULL) {
                parent::__construct($file);
            }

            function getNameFrom($cpf) {
                $sql = "SELECT name FROM submission WHERE cpf = :cpf";
                $stmt = $this->prepare($sql, ['cpf' => $cpf]);
                $stmt->execute();
                return $stmt->fetch();
            }
        }
    }

    namespace admin {
        use ConnDB;

        class CDB extends ConnDB {
            function __construct($file = NULL) {
                parent::__construct($file);
            }

            function contents() {
                $sql = "SELECT submission.cpf, submission.name FROM content LEFT JOIN submission ON (submission.cpf = content.cpf) GROUP BY content.cpf";
                $stmt = $this->prepare($sql);
                $stmt->execute();
                return $stmt->fetchAll();
            }

            function files($cpf) {
                $sql = "SELECT file.id, file.filebasename, file.fileextension, file.filekey FROM file LEFT JOIN content ON (content.file = file.id) WHERE content.cpf = :cpf";
                $stmt = $this->prepare($sql, ['cpf' => $cpf]);
                $stmt->execute();
                return $stmt->fetchAll();
            }

            function fileFromID($fileid) {
                $sql = "SELECT * FROM file WHERE file.id = :fileid";
                $stmt = $this->prepare($sql, ['fileid' => $fileid]);
                $stmt->execute();
                return $stmt->fetch();
            }
        }
    }

    namespace server {
        use ConnDB;

        class CDB extends ConnDB {
            function __construct($dblib = 'mysql') {
                parent::__construct($dblib);
            }

            function isInMaintenance() {
                $sql = "SELECT is_in_maintenance FROM server";
                $stmt = $this->prepare($sql);
                $stmt->execute();
                return $stmt->fetch()->is_in_maintenance === '1';
            }

            function isInscriptionOpen() {
                $sql = "SELECT is_user_registration_open FROM server";
                $stmt = $this->prepare($sql);
                $stmt->execute();
                return $stmt->fetch()->is_user_registration_open === '1';
            }

            function changeMaintenance($value) {
                $sql = "UPDATE server SET is_in_maintenance = :value";
                $stmt = $this->prepare($sql, ['value' => $value]);
                return $stmt->execute() === true;
            }

            function changeInscription($value) {
                $sql = "UPDATE server SET is_user_registration_open = :value";
                $stmt = $this->prepare($sql, ['value' => $value]);
                return $stmt->execute() === true;
            }

            function configurations() {
                $sql = "SELECT is_in_maintenance, is_user_registration_open FROM server";
                $stmt = $this->prepare($sql);
                $stmt->execute();
                return $stmt->fetch();
            }
        }
    }