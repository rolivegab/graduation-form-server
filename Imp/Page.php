<?php
	abstract class Page {
		public $command;
		public $message;

		function __construct() {
			$this->message = new stdClass();
		}

		function parse() {
			if(SM::isJSON('command')) {
				$this->command = SM::getJSON('command');
				$this->execute();
			} else {
				$this->setMessage('success', false);
				$this->send();
			}
		}

		function setMessage($var, $value) {
			$this->message[$var] = $value;
		}

		function send() {
			echo json_encode($this->message);
		}

		abstract function execute();
	}