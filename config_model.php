<?php

    date_default_timezone_set('America/Fortaleza');

    global $CFG;
    $CFG = new stdClass();

    $CFG->admin_user = 'admin';
    $CFG->admin_password = 'Facex@123';

    $CFG->mysqlhost = 'localhost';
    $CFG->mysqldbname = 'contentsys';
    $CFG->mysqluser = 'contentsys_user';
    $CFG->mysqlpassword = 'content@123';

    $CFG->file = '/var/www/html/graduation-form-server/database.sqlite3';
    $CFG->time = '6 de março, terça, 14:26';
    $CFG->apachegroup = 'apache';
    $CFG->webroot = '';
    
    // Indique a url pela qual o app deve ser acessado, sem colocar '/' no final.
    // $CFG->clientURL = 'http://inscricao.ead.unifacex.com.br';
    $CFG->clientURL = 'http://localhost:3000';
