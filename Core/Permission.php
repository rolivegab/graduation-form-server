<?php
    use user\CDB;
    class Permission {
        static $bool = true;
        static $sended = true;

        static function isLogged(): self {
            SM::isSESSION('logged')
                ? static::calculate(true)
                : static::calculate(false);
            return new static;
        }
        private static function calculate(bool $flag) {
            if (static::$bool === true && $flag === false) {
                static::$bool = false;
            }

            static::$sended = false;
        }
        static function v(): bool {
            $aux = static::$bool;
            static::$bool = true;
            static::$sended = true;
            return $aux;
        }
    }