<?php
    require_once( realpath(__DIR__.'/../config.php'));
    require_once( __DIR__.'/ConnDB.php');
    require_once( __DIR__.'/Permission.php');
    require_once(__DIR__.'/SM.php');
    require_once( realpath(__DIR__.'/../Imp/CDB/autoload.php'));
    require_once( __DIR__.'/SendMail.php');
    require_once( realpath(__DIR__.'/../Imp/lib.php'));

    abstract class Core {
        static $modules;

        static function initialize(array $modules = NULL) {
            static::setHeader();
            SM::parseJSON();
        }

        private static function setHeader() {
            global $CFG;
            header('Access-Control-Allow-Origin: '.$CFG->clientURL);
            header('Access-Control-Allow-Credentials: true');
            header('Content-Type: application/json');
            header('Access-Control-Allow-Headers: Content-Type');
    
            if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
                die();
            }
        }

        static function send($obj) {
            echo json_encode($obj);
            die();
        }

        static function true($obj = null) {
            if (isset($obj)) {
                echo json_encode(['success' => true, 'response' => $obj]);
            } else {
                echo json_encode(['success' => true]);
            }
            die();
        }

        static function false($obj = NULL) {
            echo json_encode(['success' => false, 'response' => $obj]);
            die();
        }

        static function redirect($page) {
            global $CFG;
            static::send([
                'redirect' => $CFG->clientURL.$page
            ]);
        }

        static function attach(string $command, Closure $f) {
            if (SM::isJSON('command') && SM::getJSON('command') === $command) {
                $r = $f();
                if($r === false) {
                    Core::false();
                } else if ($r === true) {
                    Core::true();
                } else if ($r !== NULL) {
                    Core::true($r);
                }
            }
        }
    }