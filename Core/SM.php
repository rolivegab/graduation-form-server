<?php
    class SM
    {
        public static function StartSession()
        {
            // $better_token = md5(uniqid(rand(), true));
            $status = session_status();
            if ($status == PHP_SESSION_NONE) {
                session_start();
            }
        }

        // public static function getID() {
        //     return session_id();
        // }

        // public static function setID(string $string): void {
        //     session_id($string);
        // }
        
        public static function isPOST($var) {
            return isset($_POST[$var]);
        }
        
        public static function isGET($var) {
            return isset($_GET[$var]);
        }
        
        public static function isSESSION($var) {
            static::StartSession();

            return isset($_SESSION[$var]);
        }

        public static function getGET($var) {
            return $_GET[$var];
        }
        
        
        public static function getPOST($var) {
            return $_POST[$var];
        }

        public static function getSESSION($var) {
            SM::StartSession();
            return $_SESSION[$var];
        }

        public static function setSESSION($var, $valor) {
            SM::StartSession();
            $_SESSION[$var] = $valor;
        }

        public static function closeSESSION() {
            SM::StartSession();
            session_unset();
            session_destroy();
        }
        
        public static function unsetSESSION($var) {
            SM::StartSession();
            unset($_SESSION[$var]);
        }

        public static function parseJSON() {
            static::$json = json_decode(file_get_contents('php://input', true));

            if(!empty(static::$json)) {
                static::$isjson = true;
            }
        }

        public static function isJSON($var) {
            if(!static::$isjson) {
                return false;
            }

            return isset(static::$json->$var);
        }

        public static function checkJSONMultiple(array $parameters): array {
            $errors = [];
            foreach ($parameters as $p) {
                if (is_string($p)) {
                    if (!SM::isJSON($p)) {
                        $errors[] = $p;
                    }
                } else if (is_array($p)) {
                    $errors[] = static::checkJSONMultiple($p);
                } else {
                    continue;
                }
            }
            return $errors;
        }

        public static function getJSONMultiple(array &$parameters): stdClass {
            $results = new stdClass();
            foreach ($parameters as $p) {
                if (is_string($p) && SM::isJSON($p)) {
                    $results->$p = SM::getJSON($p);
                } else if (is_object($p) && SM::isJSON($p)) {
                    $results[$p] = static::getJSONMultiple($p);
                }
            }
            return $results;
        }

        public static function checkFileMultiple(array $parameters): array {
            $errors = [];
            foreach ($parameters as $p) {
                if (is_string($p)) {
                    if (!isset($_FILES[$p])) {
                        $errors[] = $p;
                    }
                } else if (is_array($p)) {
                    $errors[] = static::checkFileMultiple($p);
                } else {
                    continue;
                }
            }
            return $errors;
        }

        public static function isFile($filename) {
            return isset($_FILES[$filename]);
        }

        public static function getFile($filename) {
            return $_FILES[$filename];
        }

        public static function getFiles() {
            return $_FILES;
        }

        public static function getFilesMultiple(array $parameters) {
            $files = [];
            foreach ($parameters as $p) {
                if (static::isFile($p)) {
                    $files[$p] = static::getFile($p);
                } else {
                    continue;
                }
            }
            return $files;
        }

        public static function getJSON($var) {
            $aux = static::$json->$var;
            return $aux;
        }

        public static function checkSESSIONMultiple(array $parameters) {
            $errors = [];
            foreach ($parameters as $p) {
                if (is_string($p)) {
                    if (!static::isSESSION($p)) {
                        $errors[] = $p;
                    }
                } else if (is_array($p)) {
                    $errors[] = static::checkSESSIONMultiple($p);
                } else {
                    continue;
                }
            }
            return $errors;
        }

        private static $json;
        private static $isjson = false;
    }