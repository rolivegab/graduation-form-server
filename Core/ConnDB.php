<?php
    require_once( realpath(__DIR__.'/../config.php'));
    require_once( realpath(__DIR__.'/Log.php'));

    class ConnDB {
        public function __construct($dblib) {
            global $CFG;
            $this->file = $CFG->file;
            $this->dblib = $dblib;
            $this->log = new Log('ConnDB');
            $this->Connect();
        }

        public function Connect() {
            try {
                if ($this->dblib === 'sqlite') {
                    $this->dbconn = new PDO("sqlite:$this->file");
                    $this->activeForeignKeys();
                } else if ($this->dblib === 'mysql') {
                    global $CFG;
                    $this->dbconn = new PDO("mysql:host=$CFG->mysqlhost;dbname=$CFG->mysqldbname;charset=utf8", $CFG->mysqluser, $CFG->mysqlpassword);
                }
            } catch(Exception $e) {
                $this->log->putL($e->getMessage());
                return false;
            }

            $this->dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->dbconn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

            return true;
        }

        protected function activeForeignKeys() {
            $this->prepare('PRAGMA foreign_keys = ON;')->execute();
        }

        public function beginTransaction() {
            $this->getDBConn()->beginTransaction();
        }

        public function rollBack() {
            $this->getDBConn()->rollBack();
        }

        public function commit() {
            $this->getDBConn()->commit();
        }

        protected function prepare($sql, $params = NULL): PDOStatement {   
            $stmt = $this->dbconn->prepare($sql);
            if($params) {
                foreach($params as $key=>$param) {
                    if(is_int($param)) {
                        $stmt->bindValue($key, $param, PDO::PARAM_INT);
                    } else if (is_string($param)) {
                        $stmt->bindValue($key, $param, PDO::PARAM_STR);
                    } else if (is_bool($param)) {
                        $stmt->bindValue($key, $param, PDO::PARAM_BOOL);
                    } else if ($param === NULL) {
                        $stmt->bindValue($key, $param);
                    }
                }
            }

            return $stmt;
        }

        protected function lastInsertID() {
            return $this->dbconn->lastInsertID();
        }

        protected function getDBConn(): PDO {
            return $this->dbconn;
        }

        private $dbconn;

        private $dbname;
        private $dbuser;
        private $dbpassword;
        private $dbhost;
        private $dblib;
    }