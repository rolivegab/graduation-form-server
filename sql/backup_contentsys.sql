-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: contentsys
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `graduation_area`
--

DROP TABLE IF EXISTS `graduation_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graduation_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graduation_area`
--

LOCK TABLES `graduation_area` WRITE;
/*!40000 ALTER TABLE `graduation_area` DISABLE KEYS */;
INSERT INTO `graduation_area` VALUES (3,'Psicologia'),(4,'Direito'),(5,'Multidisciplinar'),(6,'Pedagogia'),(7,'Sociologia'),(8,'Biologia'),(9,'Contabilidade'),(10,'Administração');
/*!40000 ALTER TABLE `graduation_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graduation_area_course`
--

DROP TABLE IF EXISTS `graduation_area_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graduation_area_course` (
  `graduationareaid` int(11) NOT NULL,
  `graduationcourseid` int(11) NOT NULL,
  PRIMARY KEY (`graduationcourseid`,`graduationareaid`),
  KEY `graduationareaid` (`graduationareaid`),
  CONSTRAINT `graduation_area_course_ibfk_1` FOREIGN KEY (`graduationareaid`) REFERENCES `graduation_area` (`id`),
  CONSTRAINT `graduation_area_course_ibfk_2` FOREIGN KEY (`graduationcourseid`) REFERENCES `graduation_course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graduation_area_course`
--

LOCK TABLES `graduation_area_course` WRITE;
/*!40000 ALTER TABLE `graduation_area_course` DISABLE KEYS */;
INSERT INTO `graduation_area_course` VALUES (3,3),(4,4),(5,5),(5,6),(5,7),(6,8),(6,9),(6,10),(7,11),(8,12),(8,13),(9,14),(10,15),(10,16),(10,17),(10,18),(10,19),(10,20),(10,21);
/*!40000 ALTER TABLE `graduation_area_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graduation_course`
--

DROP TABLE IF EXISTS `graduation_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graduation_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graduation_course`
--

LOCK TABLES `graduation_course` WRITE;
/*!40000 ALTER TABLE `graduation_course` DISABLE KEYS */;
INSERT INTO `graduation_course` VALUES (3,'Psicologia'),(4,'Direito Empresarial'),(5,'Meio ambiente e sociedade'),(6,'Elaboração de Trabalho de Conclusão de Curso'),(7,'Ética Profissional'),(8,'Organização política da Educação Básica'),(9,'Psicologia educacional'),(10,'Didática'),(11,'Sociologia'),(12,'Fisiologia Básica'),(13,'Bioquímica'),(14,'Contabilidade'),(15,'Administração Financeira'),(16,'Marketing'),(17,'Sistemas de Informação'),(18,'Comportamento Organizacional'),(19,'Empreendedorismo'),(20,'Mercado e Canais'),(21,'Comunicação Empresarial');
/*!40000 ALTER TABLE `graduation_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'conteudist'),(2,'reviewer'),(3,'manager'),(4,'admin');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `server`
--

DROP TABLE IF EXISTS `server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `server` (
  `is_in_maintenance` tinyint(1) NOT NULL,
  `maintenance_date` int(11) DEFAULT NULL,
  `is_inscription_open` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `server`
--

LOCK TABLES `server` WRITE;
/*!40000 ALTER TABLE `server` DISABLE KEYS */;
INSERT INTO `server` VALUES (0,NULL,1);
/*!40000 ALTER TABLE `server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpf` varchar(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `timecreated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cpf` (`cpf`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_email_confirmation`
--

DROP TABLE IF EXISTS `user_email_confirmation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_email_confirmation` (
  `userid` int(11) NOT NULL,
  `token` varchar(20) NOT NULL,
  `validated` tinyint(1) NOT NULL,
  `timecreated` int(11) NOT NULL,
  PRIMARY KEY (`userid`),
  CONSTRAINT `user_email_confirmation_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_email_confirmation`
--

LOCK TABLES `user_email_confirmation` WRITE;
/*!40000 ALTER TABLE `user_email_confirmation` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_email_confirmation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info` (
  `userid` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `rg` varchar(15) NOT NULL,
  `birthday` int(11) NOT NULL,
  `address` varchar(300) NOT NULL,
  `number` varchar(20) NOT NULL,
  `cep` varchar(20) NOT NULL,
  `city` varchar(50) NOT NULL,
  `tel` varchar(50) NOT NULL,
  PRIMARY KEY (`userid`),
  CONSTRAINT `user_info_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_info`
--

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `userid` int(11) NOT NULL,
  `roleid` int(11) NOT NULL,
  PRIMARY KEY (`userid`,`roleid`),
  KEY `roleid` (`roleid`),
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`),
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-05 21:18:21
