<?php
    require_once( realpath(__DIR__.'/../Core/Core.php'));
    Core::initialize();

    class stc extends ConnDB {
        function __construct($dblib = 'mysql') {
            parent::__construct($dblib);
        }

        function alterGraduationCourse() {
            $sql = "ALTER TABLE graduation_course ADD COLUMN active BOOLEAN NOT NULL DEFAULT 1";
            $stmt = $this->prepare($sql);
            $stmt->execute();
        }
    };

    $db = new stc();
    $db->alterGraduationCourse();