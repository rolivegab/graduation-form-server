<?php
    require_once( realpath(__DIR__.'/../../../../Core/Core.php'));
    Core::initialize();
    use cadastro\CDB;

    if (SM::isJSON('token')) {
        $token = SM::getJSON('token');
        $db = new CDB();
        Core::send([
            'success' => $db->setValidate($token, true)
        ]);
    }