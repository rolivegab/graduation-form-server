<?php
    require_once( realpath(__DIR__.'/../../../../Core/Core.php'));
    Core::initialize();
    use cadastro\CDB;

    Core::attach('create-user', function() {
        if (SM::isJSON('cpf') && SM::isJSON('email')) {
            $cpf = SM::getJSON('cpf');
            $email = SM::getJSON('email');

            $query = [];
            
            if (!validaCPF($cpf)) {
                $query['cpf'] = false;
            }
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $query['email'] = false;
            }

            // Error message for field validation:
            if (count($query) > 0) {
                Core::false([
                    'query' => $query,
                    'errorMsg' => 'CPF ou Email inválidos'
                ]);
            }

            $email = strtolower($email);
            
            // Verifica se o cpf já existe:
            $db = new CDB();
            if ($db->isCPFExists($cpf)) {
                Core::false([
                    'error' => 0,
                    'errorMsg' => 'O CPF já existe no banco de dados'
                ]);
            }

            // Verifica se o email já existe:
            if ($db->isEmailExists($email)) {
                Core::false([
                    'error' => 1,
                    'errorMsg' => 'O Email já existe no banco de dados'
                ]);
            }

            SM::setSESSION('cpf', $cpf);
            SM::setSESSION('email', $email);

            return true;
        }
    });