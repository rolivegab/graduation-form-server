<?php
    require_once( realpath(__DIR__.'/../../../../Core/Core.php'));
    use cadastro\CDB;
    Core::initialize();

    // Check for previous parameters:
    if (!SM::isSESSION('cpf')) {
        Core::redirect('/');
    }

    if (SM::iSJSON('command') && SM::getJSON('command') === 'create-user') {
        $parameters = [
            'name',
            'rg',
            'birthday',
            'address',
            'number',
            'cep',
            'city',
            'tel'
        ];

        $errors = SM::checkJSONMultiple($parameters);
    
        if (count($errors) > 0) {
            Core::send([
                'error' => $errors
            ]);
        }
    
        $p = SM::getJSONMultiple($parameters);
        $p->cpf = SM::getSESSION('cpf');
        $p->email = SM::getSESSION('email');
        $db = new CDB();
        if ($db->createUser($p)) {
            Core::send(['success' => true]);
        } else {
            Core::send(['success' => false]);
        }
    }