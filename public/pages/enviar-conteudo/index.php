<?php
	require_once( realpath(__DIR__.'/../../../Imp/CDB.php'));
	require_once( realpath(__DIR__.'/../../../Imp/lib.php'));
	require_once( realpath(__DIR__.'/../../../Core/SM.php'));

	use enviarConteudo\CDB;
	setHeader();

	checkLogin();
	$cpf = SM::getSESSION('cpf');

	if($_FILES) {
		$arquivo = $_FILES['arquivo'];
		$uploaddir = realpath(__DIR__.'/../../../upload');
		$separatedarquivo = explode('.', $arquivo['name']);
		$fileextension = array_pop($separatedarquivo);
		$filekey = uniqid(rand()).uniqid();
		$db = new CDB();
		if($db->insertFile($arquivo['name'], $fileextension, $filekey, $cpf, $arquivo['tmp_name'], $uploaddir)) {
			redirect('/envio-sucesso');
		} else {
			$message = 'Não foi possível enviar o arquivo, tente novamente.';
			redirect('/enviar-conteudo?success=false&error='.urlencode($message));
		}
	} else {
		checkLogin(true);		
		SM::parseJSON();
		
		$command = SM::getJSON('command');

		if($command == 'user-info') {
			$db = new CDB();
			$row = $db->getNameFrom($cpf);
			echo json_encode([
				'name' => $row->name,
				'cpf' => $cpf
			]);	
		} else {
			json_redirect('/inicio-submissao');
		}
	}