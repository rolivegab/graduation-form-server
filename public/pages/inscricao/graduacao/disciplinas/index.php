<?php
    require_once( realpath(__DIR__.'/../../../../../Core/Core.php'));
    use area\CDB;
    Core::initialize();

    Core::attach('submit-subjects', function() {
        $subjects = SM::getJSON('subjects');
        SM::setSESSION('subjects', $subjects);

        return true;
    });

    Core::attach('get-courses', function() {
        $areaid = SM::getJSON('areaid');
        $db = new CDB();
        $courses = $db->getGraduationCoursesFromArea($areaid, SM::getSESSION('roles'), 20);
        return ['courses' => $courses];
    });

    Core::attach('get-areas', function() {
        $db = new CDB();
        $areas = $db->getGraduationAreas();
        return ['areas' => $areas];
    });