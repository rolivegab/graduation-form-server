<?php
    require_once( realpath(__DIR__.'/../../../../../Core/Core.php'));
    Core::initialize();

    if(!Permission::isLogged()::v()) {
        Core::redirect('/acesso');
    }

    $params = ['graduationFor', 'posgraduationFor', 'bestDegree'];
    if (count(SM::checkJSONMultiple($params)) > 0) {
        Core::send([
            'success' => false
        ]);
    }

    $ps = SM::getJSONMultiple($params);
    foreach ($ps as $key=>$p) {
        SM::setSESSION($key, $p);
    }

    Core::send([
        'success' => true
    ]);