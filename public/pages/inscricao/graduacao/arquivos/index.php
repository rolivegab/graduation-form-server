<?php
    require_once( realpath(__DIR__.'/../../../../../Core/Core.php'));
    use inscricao_graduacao\CDB;
    Core::initialize(['sendmail']);

    if (!Permission::isLogged()::v()) {
        Core::redirect('/acesso');
    }

    $fileParams = ['lattes', 'rg', 'cpf', 'elector', 'residence', 'graduationFront'];
    if (count(SM::checkFileMultiple($fileParams)) > 0) {
        Core::send([
            'success' => false
        ]);
    }
    array_push($fileParams, 'graduationVerse', 'posgraduationFront', 'posgraduationVerse');
    $files = SM::getFilesMultiple($fileParams);

    $otherFiles = [];
    foreach (SM::getFiles() as $key=>$file) {
        if (strpos($key, 'otherFiles') !== false) {
            $otherFiles[] = $file;
        }
    }

    $db = new CDB();

    $params = ['subjects', 'graduationFor', 'posgraduationFor', 'employeedBefore', 'bestDegree', 'teacherExperience', 'eadExperience', 'contentExperience', 'otherExperiences'];
    $errors = SM::checkSESSIONMultiple($params);
    if (count($errors) > 0) {
        Core::send([
            'success' => false,
            'errorMsg' => $errors
        ]);
    }

    if ($db->submitInscription(
        SM::getSESSION('user')['id'],
        SM::getSESSION('bestDegree'),
        SM::getSESSION('graduationFor'),
        SM::getSESSION('posgraduationFor'),
        SM::getSESSION('employeedBefore'),
        SM::getSESSION('teacherExperience'),
        SM::getSESSION('eadExperience'),
        SM::getSESSION('contentExperience'),
        SM::getSESSION('otherExperiences'),
        SM::getSESSION('roles'),
        SM::getSESSION('subjects'),
        $files,
        $otherFiles
    )) {
        Core::send([
            'success' => true
        ]);
    }