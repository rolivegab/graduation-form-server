<?php
    require_once( realpath(__DIR__.'/../../../../../Core/Core.php'));
    Core::initialize();

    Core::attach('submit-roles', function() {
        if (!Permission::isLogged()::v()) {
            Core::redirect('/acesso');
        }
    
        $params = ['roles'];
        $error = SM::checkJSONMultiple($params);
        if(count($error) > 0) {
            Core::false([
                'success' => false,
                'errorMsg' => $error
            ]);
        }
    
        $ps = SM::getJSONMultiple($params);
        foreach($ps as $key=>$p) {
            SM::setSESSION($key, $p);
        }
    
        return true;
    });