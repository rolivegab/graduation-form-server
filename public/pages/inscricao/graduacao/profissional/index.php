<?php
    require_once( realpath(__DIR__.'/../../../../../Core/Core.php'));
    Core::initialize();

    if (!Permission::isLogged()::v()) {
        Core::redirect('/acesso');
    }

    $params = ['employeedBefore', 'teacherExperience', 'eadExperience', 'contentExperience', 'otherExperiences'];
    $error = SM::checkJSONMultiple($params);
    if(count($error) > 0) {
        Core::send([
            'success' => false,
            'errorMsg' => $error
        ]);
    }

    $ps = SM::getJSONMultiple($params);
    foreach($ps as $key=>$p) {
        SM::setSESSION($key, $p);
    }

    Core::send([
        'success' => true
    ]);