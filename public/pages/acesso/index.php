<?php
    require_once( realpath(__DIR__.'/../../../Core/Core.php'));
    use user\CDB;
    Core::initialize();

    // Verifica se os parâmetros foram enviados:
    if(count(SM::checkJSONMultiple(['cpf', 'email'])) > 0) {
        Core::send([
            'success' => false,
            'errorMsg' => 'CPF ou E-mail incorretos'
        ]);
    }

    $db = new CDB();
    $cpf = SM::getJSON('cpf');
    $email = SM::getJSON('email');
    $userid = $db->checkLogin($cpf, $email);
    if ($userid) {
        $roles = [];
        array_map(function($row) use (&$roles) {
            $roles[$row->name] = null;
        }, $db->getRolesFromUser($userid));
        SM::setSESSION('logged', true);
        SM::setSESSION('user', [
            'id' => $userid,
            'isEmailValidated' => $db->isEmailValidated($userid),
            'roles' => $roles,
            'email' => $db->getMail($userid),
            'name' => $db->getName($userid)
        ]);
        Core::send([
            'success' => true
        ]);
    } else {
        Core::send([
            'success' => false,
            'errorMsg' => 'CPF ou E-mail inválidos!'
        ]);
    }