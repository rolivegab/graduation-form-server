<?php
    require_once( realpath(__DIR__.'/../../../Imp/lib.php'));
    require_once( realpath(__DIR__.'/../../../Core/SM.php'));
    require_once( realpath(__DIR__.'/../../../config.php'));
    require_once( realpath(__DIR__.'/../../../Core/ConnDB.php'));
    require_once( realpath(__DIR__.'/../../../Imp/CDB.php'));
    use server\CDB;

    setHeader();
    SM::parseJSON();
    if (!SM::isJSON('command')) {
        return;
    }

    $command = SM::getJSON('command');
    global $CFG;

    if ($command == 'isInMaintenance') {
        $db = new CDB();

        echo json_encode([
            'isInMaintenance' => $db->isInMaintenance()
        ]);
    }