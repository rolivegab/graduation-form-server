<?php
    require_once( realpath(__DIR__.'/../../../Core/Core.php'));
    Core::initialize();
    
    Core::attach('logout', function() {
        SM::closeSESSION();
        return true;
    });