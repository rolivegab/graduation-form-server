<?php
    require_once( realpath(__DIR__.'/../../../../Core/Core.php'));
    Core::initialize();
    use user\CDB;

    if (!SM::isJSON('command')) {
        Core::send([
            'success' => false
        ]);
    }

    if (!Permission::isLogged()::v()) {
        Core::send([
            'success' => false,
            'logged' => false
        ]);
    }


    $command = SM::getJSON('command');
    if ($command === 'user-status') {
        $db = new CDB();
        $userid = SM::getSESSION('user')['id'];
        Core::send([
            'success' => true,
            'logged' => Permission::isLogged()::v(),
            'isEmailValidated' => $db->isEmailValidated($userid),
            'hasGraduationInscription' => $db->hasGraduationInscription($userid),
            'fullname' => SM::getSESSION('user')['name']
        ]);
    }