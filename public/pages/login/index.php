<?php
	require_once( realpath(__DIR__.'/../../../Core/ConnDB.php'));
	require_once( realpath(__DIR__.'/../../../Imp/CDB.php'));
	require_once( realpath(__DIR__.'/../../../Imp/lib.php'));
	require_once( realpath(__DIR__.'/../../../Core/SM.php'));

	use login\CDB;

	if(SM::isPOST('cpf')) {
		$cpf = SM::getPOST('cpf');
		$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
		$cpf = implode('', explode('-', implode('', explode('.', $cpf))));

		$query = [];

		// Validate fields:
		// CPF:
		if (!validaCPF($cpf)) {
			$query['cpf'] = false;
		}

		if (!$email) {
			$query['email'] = false;
		}

		$email = strtolower($email);

		// Error message for field validation:
		if(count($query) > 0) {
			redirect('/inicio-submissao/?'.http_build_query($query));
			die();
		}

		$db = new CDB();
		
		// Verifica se o login está correto:
		if ($db->checkLogin($cpf, $email)) {
			SM::setSESSION('cpf', $cpf);
			redirect('/enviar-conteudo/');
		} else {
			$query['login'] = false;
			redirect('/inicio-submissao/?'.http_build_query($query));
			die();
		}

		// Realiza o login:
	}