<?php
    require_once( realpath(  __DIR__.'/../../../../../Core/Core.php'));
    use convert\SCDB;
    use user\CDB;
    Core::initialize();

    Core::attach('get-l-submissions', function() {
        $sdb = new SCDB();

        Core::true([
            'l_subs' => $sdb->getSubmissions()
        ]);
    });

    Core::attach('get-m-users', function() {
        $db = new CDB();

        Core::true([
            'm_users' => $db->getUsers()
        ]);
    });