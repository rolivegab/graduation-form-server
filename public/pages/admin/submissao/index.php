<?php
    require_once( realpath(__DIR__.'/../../../../Imp/CDB.php'));
    require_once( realpath(__DIR__.'/../../../../Imp/lib.php'));
    require_once( realpath(__DIR__.'/../../../../Core/SM.php'));
    require_once( realpath(__DIR__.'/../../../../config.php'));

    use admin\CDB;

    setHeader();

    SM::parseJSON();

    if (!SM::isJSON('command')) {
        send(['success' => false]);
        return;
    }

    checkAdminLogin(true);

    $command = SM::getJSON('command');

    if ($command == 'content') {
        $db = new CDB();
        $contents = $db->contents();
        send([
            'contents' => $contents
        ]);
    } else if ($command == 'files' && SM::isJSON('cpf')) {
        $cpf = SM::getJSON('cpf');
        $db = new CDB();
        $files = $db->files($cpf);
        send([
            'files' => $files
        ]);
    }