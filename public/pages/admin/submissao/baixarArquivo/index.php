<?php
    require_once( realpath(__DIR__.'/../../../../../Imp/CDB.php'));
    require_once( realpath(__DIR__.'/../../../../../Imp/lib.php'));
    require_once( realpath(__DIR__.'/../../../../../Core/SM.php'));

    checkAdminLogin();

    use admin\CDB;

    // Verifica se o arquivo do ID pedido existe no banco
    // Caso exista, verifica se existe na pasta, se sim, baixa, 
    // Caso não exista na pasta, apresenta uma mensagem de erro.
    if (!SM::isGET('arquivo')) {
        die();
    }

    $fileid = SM::getGET('arquivo');
    $db = new CDB();
    $file = $db->fileFromID($fileid);
    if ($file === false) {
        echo 'Arquivo não existe no banco';
    } else {
        // Verifica se o arquivo existe na pasta:
        $uploadfolder = realpath(__DIR__.'/../../../../../upload');
        if (!$uploadfolder) {
            echo 'upload folder errado!';
        }

        $pFile = $uploadfolder.'/'.$file->filekey;

        if (is_file($pFile)) {
            //Get file type and set it as Content Type
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            header('Content-Type: '.finfo_file($finfo, $pFile));
            finfo_close($finfo);

            //Use Content-Disposition: attachment to specify the filename
            header('Content-Disposition: attachment; filename='.basename($file->filebasename));

            //No cache
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');

            //Define file size
            header('Content-Length: '.filesize($pFile));

            readfile($pFile);
            exit;
        } else {
            echo 'Erro! Esse arquivo não existe no servidor.<br>Já esperava por algo assim, enfim.<br>Anote o número <b><u>'.$fileid.'</u></b> em algum lugar e depois me mostre.<br>Obrigado!<br><br>Gabriel.';
        }
    }