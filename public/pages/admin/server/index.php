<?php
    require_once( realpath(__DIR__.'/../../../../Imp/lib.php'));
    require_once( realpath(__DIR__.'/../../../../Core/ConnDB.php'));
    require_once( realpath(__DIR__.'/../../../../Imp/CDB.php'));
    require_once( realpath(__DIR__.'/../../../../Core/SM.php'));
    use server\CDB;

    setHeader();
    SM::parseJSON();
    checkAdminLogin(true);
    if (SM::isJSON('command')) {
        $command = SM::getJSON('command');

        if ($command === 'changeConfiguration') {
            $db = new CDB();

            if (SM::getJSON('name') === 'maintenance') {
                echo json_encode([
                    'success' => $db->changeMaintenance(SM::getJSON('value'))
                ]);
            } else if (SM::getJSON('name') === 'inscription') {
                echo json_encode([
                    'success' => $db->changeInscription(SM::getJSON('value'))
                ]);
            }
        } else if ($command === 'configurations') {
            $db = new CDB();

            $c = $db->configurations();

            echo json_encode([
                'configurations' => [
                    'maintenance' => $c->is_in_maintenance === '1',
                    'inscription' => $c->is_user_registration_open === '1'
                ]
            ]);
        }
    }