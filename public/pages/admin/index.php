<?php
    require_once( realpath(__DIR__.'/../../../Core/Core.php'));
    require_once( realpath(__DIR__.'/../../../config.php'));
    Core::initialize();

    Core::attach('admin-login', function() {
        global $CFG;
        $p = ['username', 'password'];
        if(count(SM::checkJSONMultiple($p)) > 0) {
            return false;
        }

        $h = SM::getJSONMultiple($p);
        if ($h->username === $CFG->admin_user && $h->password === $CFG->admin_password) {
            SM::setSESSION('admin', true);
            return true;
        }
    });