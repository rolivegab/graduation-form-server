<?php
    require_once( realpath(__DIR__.'/../Core/Core.php'));
    Core::initialize();
    use database\CDB;

    if ($_SERVER['REQUEST_METHOD'] === 'GET' && SM::isGET('command') && SM::getGET('command') === 'insert') {
        $subjects = [
            'Psicologia' => [
                'Psicologia'
            ],
            'Direito' => [
                'Direito Empresarial'
            ],
            'Multidisciplinar' => [
                'Meio ambiente e sociedade',
                'Elaboração de Trabalho de Conclusão de Curso',
                'Ética Profissional'
            ],
            'Pedagogia' => [
                'Organização política da Educação Básica',
                'Psicologia educacional',
                'Didática'
            ],
            'Sociologia' => [
                'Sociologia'
            ],
            'Biologia' => [
                'Fisiologia Básica',
                'Bioquímica'
            ],
            'Contabilidade' => [
                'Contabilidade'
            ],
            'Administração' => [
                'Administração Financeira',
                'Marketing',
                'Sistemas de Informação',
                'Comportamento Organizacional',
                'Empreendedorismo',
                'Mercado e Canais',
                'Comunicação Empresarial',
                'Logística'
            ]
        ];

        $db = new CDB();
        $db->insertSubjects($subjects);
    }